#include <linux/module.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/i2c.h>
#include <linux/delay.h> /* msleep() */

/* Meta Information */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("A driver for my simple I2C LCD");

static struct i2c_client *lcd_client;
static struct proc_dir_entry *proc_file;

/* Declate the probe and remove functions */
static int my_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id);
static void my_i2c_remove(struct i2c_client *client);

static struct of_device_id my_driver_ids[] = {
	{
		.compatible = "brightlight,myi2c",
	}, { /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, my_driver_ids);

static struct i2c_device_id my_i2c[] = {
	{"my_i2c", 0},
	{ },
};
MODULE_DEVICE_TABLE(i2c, my_i2c);

static struct i2c_driver my_driver = {
	.probe = my_i2c_probe,
	.remove = my_i2c_remove,
	.id_table = my_i2c,
	.driver = {
		.name = "my_i2c",
		.of_match_table = my_driver_ids,
	},
};

/**
 * @brief Set up LCD I2C
*/

void LCD_Write_Data(char data)
{
	char data_u, data_l;
	uint8_t data_t[4] = {0}, i = 0;

	data_u = data&0xf0;
	data_l = (data<<4)&0xf0;
	data_t[0] = data_u|0x0d;  //en=1, rs=0
	data_t[1] = data_u|0x09;  //en=0, rs=0
	data_t[2] = data_l|0x0d;  //en=1, rs=0
	data_t[3] = data_l|0x09;  //en=0, rs=0

	for(i = 0; i < 4; i++)
	{
	    i2c_smbus_write_byte(lcd_client, data_t[i]);
	}
}

void LCD_Write_Control(char cmd)
{
	char data_u, data_l;
	uint8_t data_t[4] = {0}, i = 0;

	data_u = cmd&0xf0;
	data_l = (cmd<<4)&0xf0;
	data_t[0] = data_u|0x0C;  //en=1, rs=0
	data_t[1] = data_u|0x08;  //en=0, rs=0
	data_t[2] = data_l|0x0C;  //en=1, rs=0
	data_t[3] = data_l|0x08;  //en=0, rs=0

	for(i = 0; i < 4; i++)
	{
	    i2c_smbus_write_byte(lcd_client, data_t[i]);
	}
}

void LCD_Config(void)
{
	/* Set 4-bits interface */
	LCD_Write_Control(0x33);		 
	msleep(1);
	LCD_Write_Control(0x32);
	msleep(1);
	/* Start to set LCD function */
	LCD_Write_Control(0x28);
	msleep(1);	
	/* clear LCD */
	LCD_Write_Control(0x01);
	msleep(1);
	/* set entry mode */
	LCD_Write_Control(0x06);	
	msleep(1);
	/* set display to on */	
	LCD_Write_Control(0x0C);	
	msleep(1);
	/* move cursor to home and set data address to 0 */
	LCD_Write_Control(0x02);	
	msleep(1);
	LCD_Write_Control(0x80);
}

void LCD_Write_String (char *str)
{
	while(*str) LCD_Write_Data(*str++);
}

void LCD_Goto_XY (uint8_t row, uint8_t col)
{
	uint8_t pos_Addr;
	if(row == 1) 
    {
		pos_Addr = 0x80 + row - 1 + col;
	}
	else
    {
		pos_Addr = 0x80 | (0x40 + col);
	}
	LCD_Write_Control(pos_Addr);
}

/**
 * @brief Update timing between to ADC reads
 */
static ssize_t my_write(struct file *File, const char *user_buffer, size_t count, loff_t *offs) 
{
	char lcd_buffer[17];
	int to_copy, not_copy, delta;

	/* Get amount of data to copy */
	to_copy = min(count, sizeof(lcd_buffer));

	/* Copy data to user */
	not_copy = copy_from_user(lcd_buffer, user_buffer, to_copy);
		
	/* Calculate data */
	delta = to_copy - not_copy;

	/* Display the new data */
	LCD_Write_Control(0x01);
	msleep(10);
	LCD_Goto_XY(1, 0);
	// LCD_Write_String(lcd_buffer);
	for(int i = 0; i < to_copy - 1; i++)
	{
		LCD_Write_Data(lcd_buffer[i]);
	}

	return delta;
}

static struct proc_ops fops = {
	.proc_write = my_write,
};

/**
 * @brief This function is called on loading the driver 
 */
static int my_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id) 
{
	printk("Hello, Kernel!\n");
	printk("dt_i2c - Now I am in the Probe function!\n");

	if(client->addr != 0x27) 
	{
		printk("dt_i2c - Wrong I2C address!\n");
		return -1;
	}

	/* Stores a pointer address to the current I2C device for use in writing data to the LCD */
	lcd_client = client;
		
	/* Creating procfs file */
	proc_file = proc_create("myi2c", 0666, NULL, &fops);
	if(proc_file == NULL) 
	{
		printk("dt_i2c - Error creating /proc/myi2c\n");
		return -ENOMEM;
	}

    /*LCD Init */
    LCD_Config();
    LCD_Goto_XY(1, 4);
    LCD_Write_String("AIoT Lab");
	LCD_Goto_XY(2, 4);
    LCD_Write_String("Loi Tran");
	
	return 0;
}

/**
 * @brief This function is called on unloading the driver 
 */
static void my_i2c_remove(struct i2c_client *client) 
{
	/* Clear display */
	LCD_Write_Control(0x01);
	printk("dt_i2c - Now I am in the Remove function!\n");
	proc_remove(proc_file);
	printk("Bye, Kernel!\n");
}

/* This will create the init and exit function automatically */
module_i2c_driver(my_driver);