#ifndef _TFT_DISPLAY_H_
#define _TFT_DISPLAY_H_

#include "linux/module.h"
#include "linux/init.h"
#include "linux/proc_fs.h"
#include "linux/spi/spi.h"
#include "linux/gpio.h"
#include "linux/delay.h"
#include "linux/of.h"

#define GRAYBLUE       	        0X5458
#define WHITE       	        0xFFFF
#define BLACK      		        0x0000	  
#define BLUE       		        0x001F  
#define BRED        	        0XF81F
#define GRED 			        0XFFE0
#define GBLUE			        0X07FF
#define RED         	        0xF800
#define MAGENTA     	        0xF81F
#define GREEN       	        0x07E0
#define CYAN        	        0x7FFF
#define YELLOW     	 	        0xFFE0
#define BROWN 			        0XBC40 
#define BRRED 			        0XFC07 
#define GRAY  			        0X8430

#define TFT_WIDTH 				240
#define TFT_HEIGHT 				320

#define TFT_DC_PIN  		    535    	// Data/Command pin is GPIO 23
#define TFT_RST_PIN 		    536    	// Reset pin is GPIO 24

#define TFT_DC_SET				gpio_direction_output(TFT_DC_PIN, 1)
#define TFT_RST_SET				gpio_direction_output(TFT_RST_PIN, 1)
#define TFT_DC_RESET			gpio_direction_output(TFT_DC_PIN, 0)
#define TFT_RST_RESET			gpio_direction_output(TFT_RST_PIN, 0)

extern struct spi_device *spi_tft;

struct TFT_SPI_INFO_t {
	uint16_t width;
	uint16_t height;
	uint16_t setxcmd;
	uint16_t setycmd;
	uint16_t wramcmd;
};

void TFT_GPIO_Init_ResDc(void);
void TFT_GPIO_DeInit_ResDc(void);
int  TFT_Write(unsigned char *buf, uint16_t len);
void TFT_WriteCommand(char data);
void TFT_WriteData(char data);
void TFT_WriteReg(uint8_t reg, uint8_t data);
void TFT_WriteData_16Bit(uint16_t data);
void TFT_ShowChar(uint16_t x, uint16_t y, uint16_t fc,  uint16_t bc, uint8_t num, uint8_t size, uint8_t mode);
void TFT_ShowString(uint16_t x, uint16_t y, uint16_t fc, uint16_t bc, uint8_t size, uint8_t *p, uint8_t mode);
void TFT_SetWindows(uint16_t x_begin, uint16_t y_begin, uint16_t x_end, uint16_t y_end);
void TFT_DrawPoint(uint16_t x, uint16_t y);
void TFT_SetCursor(uint16_t x, uint16_t y);
void TFT_SetRotation(uint8_t value);
void TFT_Clear(uint16_t color);
void TFT_ClearFrame(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);
int  TFT_DisplayInit(void);

#endif
