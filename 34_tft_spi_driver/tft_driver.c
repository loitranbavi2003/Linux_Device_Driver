#include "tft_display.h"

MODULE_LICENSE("GPL"); // General Public License
MODULE_AUTHOR("Loi Tran");
MODULE_DESCRIPTION("A driver for TFT SPI");

static struct proc_dir_entry *proc_file;

static int 		my_spi_probe(struct spi_device *spi);
static void 	my_spi_remove(struct spi_device *spi);
static ssize_t 	my_write(struct file *File, const char *user_buffer, size_t count, loff_t *offs);

static const struct of_device_id my_of_device_ids[] = {
	{
		.compatible = "brightlight,myspi",
	},
	{}
};
MODULE_DEVICE_TABLE(of, my_of_device_ids);

static const struct spi_device_id my_spi_device_id[] = {
	{"brightlight,myspi", 0},
	{}
};
MODULE_DEVICE_TABLE(spi, my_spi_device_id);

static struct spi_driver my_spi_driver = {
	.driver = {
		.name = "my_spi_driver",
		.owner = THIS_MODULE,
		.of_match_table = my_of_device_ids,
	},
	.probe = my_spi_probe,
	.remove = my_spi_remove,
	.id_table = my_spi_device_id
};

static struct proc_ops fops = {
	.proc_write = my_write,
};

/**
	@brief This function is called on loading the kernel
 */
static int my_spi_probe(struct spi_device *spi)
{
	printk(KERN_INFO "Hello Kernel!\n");

	spi_tft = spi;

	/* Creating procfs file */
	proc_file = proc_create("my_spi", 0666, NULL, &fops);
	if(proc_file == NULL)
	{
		printk(KERN_INFO "spi_driver: Error creating /proc/my_spi\n");
		return -ENOMEM;
	}

	/* Register GPIO */
	TFT_GPIO_Init_ResDc();
	/* TFT Init */
	TFT_DisplayInit();
	/* TFT Display */
	TFT_ShowString(40, 10, WHITE, BLACK, 16, "Reset", 1);

	return 0;
}

/**
	@brief This function is called on unloading the kernel
 */
static void my_spi_remove(struct spi_device *spi)
{
	/* Deregister GPIO */
	TFT_GPIO_DeInit_ResDc();
	proc_remove(proc_file);
	printk(KERN_INFO "Bye Kernel!\n");
}

static ssize_t my_write(struct file *File, const char *user_buffer, size_t count, loff_t *offs)
{
	char tft_buffer[255];
	int to_copy, not_copy, delta;

	/* Get amount of data to copy */
	to_copy = min(count, sizeof(tft_buffer));

	/* Copy data to user */
	not_copy = copy_from_user(tft_buffer, user_buffer, to_copy);

	/* Delete the previous letter */
	TFT_ClearFrame(40, 40, 300, 100, BLACK);
	/* Display text coped from user */
	TFT_ShowString(40, 40, RED, WHITE, 16, tft_buffer, 1);

	delta = to_copy - not_copy;
	return delta;
}

/* This will create the init and exit function automatically */
module_spi_driver(my_spi_driver);