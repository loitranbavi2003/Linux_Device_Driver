#include <linux/module.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/spi/spi.h>
#include <linux/gpio.h>
#include <linux/delay.h> 
#include <linux/of.h>

/* Meta Information */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("A driver for my simple SPI TFT");

static struct spi_device *spi_oled;
static struct proc_dir_entry *proc_file;

/* Declate the probe and remove functions */
static int my_spi_probe(struct spi_device *spi);
static void my_spi_remove(struct spi_device *spi);

static const struct of_device_id my_driver_ids[] = {
	{
		.compatible = "brightlight,myspi",
	}, { /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, my_driver_ids);

static const struct spi_device_id my_spi[] = {
    { "brightlight,myspi", 0 },
    { }
};
MODULE_DEVICE_TABLE(spi, my_spi);

static struct spi_driver my_driver = {
  .driver = {
		.name = "my_spi",
    .owner = THIS_MODULE,
		.of_match_table = my_driver_ids,
	},
	.probe = my_spi_probe,
	.remove = my_spi_remove,
	.id_table = my_spi
};

#define TFT_RST_PIN 		    536    	// Reset pin is GPIO 24
#define TFT_DC_PIN  		    535    	// Data/Command pin is GPIO 23

#define TFT_MAX_SEG 		    128    	// Maximum segment
#define TFT_MAX_LINE 		    7    	  // Maximum line
#define TFT_FONT_SIZE       5    	  // Default font size

/*
** Array Variable to store the letters.
*/ 
static const unsigned char TFT_font[][TFT_FONT_SIZE] = 
{
  {},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},
  {},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},
  {0x00, 0x00, 0x00, 0x00, 0x00},   // space
  {0x00, 0x00, 0x2f, 0x00, 0x00},   // !
  {0x00, 0x07, 0x00, 0x07, 0x00},   // "
  {0x14, 0x7f, 0x14, 0x7f, 0x14},   // #
  {0x24, 0x2a, 0x7f, 0x2a, 0x12},   // $
  {0x23, 0x13, 0x08, 0x64, 0x62},   // %
  {0x36, 0x49, 0x55, 0x22, 0x50},   // &
  {0x00, 0x05, 0x03, 0x00, 0x00},   // '
  {0x00, 0x1c, 0x22, 0x41, 0x00},   // (
  {0x00, 0x41, 0x22, 0x1c, 0x00},   // )
  {0x14, 0x08, 0x3E, 0x08, 0x14},   // *
  {0x08, 0x08, 0x3E, 0x08, 0x08},   // +
  {0x00, 0x00, 0xA0, 0x60, 0x00},   // ,
  {0x08, 0x08, 0x08, 0x08, 0x08},   // -
  {0x00, 0x60, 0x60, 0x00, 0x00},   // .
  {0x20, 0x10, 0x08, 0x04, 0x02},   // /

  {0x3E, 0x51, 0x49, 0x45, 0x3E},   // 0
  {0x00, 0x42, 0x7F, 0x40, 0x00},   // 1
  {0x42, 0x61, 0x51, 0x49, 0x46},   // 2
  {0x21, 0x41, 0x45, 0x4B, 0x31},   // 3
  {0x18, 0x14, 0x12, 0x7F, 0x10},   // 4
  {0x27, 0x45, 0x45, 0x45, 0x39},   // 5
  {0x3C, 0x4A, 0x49, 0x49, 0x30},   // 6
  {0x01, 0x71, 0x09, 0x05, 0x03},   // 7
  {0x36, 0x49, 0x49, 0x49, 0x36},   // 8
  {0x06, 0x49, 0x49, 0x29, 0x1E},   // 9

  {0x00, 0x36, 0x36, 0x00, 0x00},   // :
  {0x00, 0x56, 0x36, 0x00, 0x00},   // ;
  {0x08, 0x14, 0x22, 0x41, 0x00},   // <
  {0x14, 0x14, 0x14, 0x14, 0x14},   // =
  {0x00, 0x41, 0x22, 0x14, 0x08},   // >
  {0x02, 0x01, 0x51, 0x09, 0x06},   // ?
  {0x32, 0x49, 0x59, 0x51, 0x3E},   // @

  {0x7C, 0x12, 0x11, 0x12, 0x7C},   // A
  {0x7F, 0x49, 0x49, 0x49, 0x36},   // B
  {0x3E, 0x41, 0x41, 0x41, 0x22},   // C
  {0x7F, 0x41, 0x41, 0x22, 0x1C},   // D
  {0x7F, 0x49, 0x49, 0x49, 0x41},   // E
  {0x7F, 0x09, 0x09, 0x09, 0x01},   // F
  {0x3E, 0x41, 0x49, 0x49, 0x7A},   // G
  {0x7F, 0x08, 0x08, 0x08, 0x7F},   // H
  {0x00, 0x41, 0x7F, 0x41, 0x00},   // I
  {0x20, 0x40, 0x41, 0x3F, 0x01},   // J
  {0x7F, 0x08, 0x14, 0x22, 0x41},   // K
  {0x7F, 0x40, 0x40, 0x40, 0x40},   // L
  {0x7F, 0x02, 0x0C, 0x02, 0x7F},   // M
  {0x7F, 0x04, 0x08, 0x10, 0x7F},   // N
  {0x3E, 0x41, 0x41, 0x41, 0x3E},   // O
  {0x7F, 0x09, 0x09, 0x09, 0x06},   // P
  {0x3E, 0x41, 0x51, 0x21, 0x5E},   // Q
  {0x7F, 0x09, 0x19, 0x29, 0x46},   // R
  {0x46, 0x49, 0x49, 0x49, 0x31},   // S
  {0x01, 0x01, 0x7F, 0x01, 0x01},   // T
  {0x3F, 0x40, 0x40, 0x40, 0x3F},   // U
  {0x1F, 0x20, 0x40, 0x20, 0x1F},   // V
  {0x3F, 0x40, 0x38, 0x40, 0x3F},   // W
  {0x63, 0x14, 0x08, 0x14, 0x63},   // X
  {0x07, 0x08, 0x70, 0x08, 0x07},   // Y
  {0x61, 0x51, 0x49, 0x45, 0x43},   // Z

  {0x00, 0x7F, 0x41, 0x41, 0x00},   // [
  {0x55, 0xAA, 0x55, 0xAA, 0x55},   // \'
  {0x00, 0x41, 0x41, 0x7F, 0x00},   // ]
  {0x04, 0x02, 0x01, 0x02, 0x04},   // ^
  {0x40, 0x40, 0x40, 0x40, 0x40},   // _
  {0x00, 0x03, 0x05, 0x00, 0x00},   // `

  {0x20, 0x54, 0x54, 0x54, 0x78},   // a
  {0x7F, 0x48, 0x44, 0x44, 0x38},   // b
  {0x38, 0x44, 0x44, 0x44, 0x20},   // c
  {0x38, 0x44, 0x44, 0x48, 0x7F},   // d
  {0x38, 0x54, 0x54, 0x54, 0x18},   // e
  {0x08, 0x7E, 0x09, 0x01, 0x02},   // f
  {0x18, 0xA4, 0xA4, 0xA4, 0x7C},   // g
  {0x7F, 0x08, 0x04, 0x04, 0x78},   // h
  {0x00, 0x44, 0x7D, 0x40, 0x00},   // i
  {0x40, 0x80, 0x84, 0x7D, 0x00},   // j
  {0x7F, 0x10, 0x28, 0x44, 0x00},   // k
  {0x00, 0x41, 0x7F, 0x40, 0x00},   // l
  {0x7C, 0x04, 0x18, 0x04, 0x78},   // m
  {0x7C, 0x08, 0x04, 0x04, 0x78},   // n
  {0x38, 0x44, 0x44, 0x44, 0x38},   // o
  {0xFC, 0x24, 0x24, 0x24, 0x18},   // p
  {0x18, 0x24, 0x24, 0x18, 0xFC},   // q
  {0x7C, 0x08, 0x04, 0x04, 0x08},   // r
  {0x48, 0x54, 0x54, 0x54, 0x20},   // s
  {0x04, 0x3F, 0x44, 0x40, 0x20},   // t
  {0x3C, 0x40, 0x40, 0x20, 0x7C},   // u
  {0x1C, 0x20, 0x40, 0x20, 0x1C},   // v
  {0x3C, 0x40, 0x30, 0x40, 0x3C},   // w
  {0x44, 0x28, 0x10, 0x28, 0x44},   // x
  {0x1C, 0xA0, 0xA0, 0xA0, 0x7C},   // y
  {0x44, 0x64, 0x54, 0x4C, 0x44},   // z

  {0x00, 0x10, 0x7C, 0x82, 0x00},   // {
  {0x00, 0x00, 0xFF, 0x00, 0x00},   // |
  {0x00, 0x82, 0x7C, 0x10, 0x00},   // }
  {0x00, 0x06, 0x09, 0x09, 0x06}    // ~ (Degrees)
};

/**
	@brief This function Initializes and Configures the Reset and DC Pin
*/
static void TFT_GPIO_Init_ResDc(void)
{
  //Requesting the Reset GPIO
  if( gpio_request(TFT_RST_PIN, "TFT_RST_PIN" ) < 0)
  {
    printk("dt_spi - ERROR: Reset GPIO %d request\n", TFT_RST_PIN);
  }
  //configure the Reset GPIO as output
  gpio_direction_output(TFT_RST_PIN, 1);

  //Requesting the DC GPIO
  if( gpio_request(TFT_DC_PIN, "TFT_DC_PIN" ) < 0)
  {
    printk("dt_spi - ERROR: DC GPIO %d request\n", TFT_DC_PIN);
    gpio_free(TFT_RST_PIN);   // free the reset GPIO
  }
  //configure the Reset GPIO as output
  gpio_direction_output(TFT_DC_PIN, 1);
}

/**
	@brief This function De-initializes the Reset and DC Pin
*/
static void TFT_GPIO_DeInit_ResDc(void)
{
  gpio_set_value(TFT_DC_PIN, 0);
  gpio_set_value(TFT_RST_PIN, 0);
  gpio_free(TFT_DC_PIN);          // free the DC GPIO
  gpio_free(TFT_RST_PIN);         // free the RES GPIO
  printk("dt_spi - Free GPIO Res and Dc\n");
}

/**
	@brief This function writes the value to the RES GPIO
*/
static void TFT_SetValueGpioRES(uint8_t value)
{
	gpio_set_value(TFT_RST_PIN, value);
}

/**
	@brief This function writes the value to the DC GPIO
*/
static void TFT_SetValueGpioDC(uint8_t value)
{
	gpio_set_value(TFT_DC_PIN, value);
}

/**
  @brief This function writes the 1-byte data to the slave device using SPI.
*/
static int TFT_Write(unsigned char *buf, uint16_t len)
{
    int status;

    status = spi_write(spi_oled, buf, len);
    return status;
}

/**
 * @brief Send a byte to the command register
*/
static void TFT_WriteCommand(char data)
{
    int ret;
	TFT_SetValueGpioDC(0x00);

    ret = TFT_Write(&data, 1);
	if(ret < 0) {
		printk("Error! Could not WriteCommand from function spi_write\n");
	}
}

/**
 * @brief Send data
*/
static void TFT_WriteData(char data)
{
    int ret; 
	TFT_SetValueGpioDC(0x01);

    ret = TFT_Write(&data, 1);
	if(ret < 0) {
		printk("Error! Could not WriteData from function spi_write\n");
	}
}

/**
  @brief Send character data
*/
static void TFT_PrintChar(char data)
{
	uint8_t data_byte;

	for(int i = 0; i < TFT_FONT_SIZE; i++)
	{
		data_byte = TFT_font[data][i];
        TFT_WriteData(data_byte);
	}
}

/**
  @brief Send string data
*/
static void TFT_String(unsigned char *str)
{
	while(*str)
	{
		TFT_PrintChar(*str++);
	}
}

/**
  @brief Set row and column display
*/
static void TFT_SetCursor(uint8_t row, uint8_t column)
{
  if ((row < TFT_MAX_LINE) && (column < TFT_MAX_SEG))
  {
    uint8_t page = row;
    uint8_t lower_column = column & 0x0F;
    uint8_t higher_column = (column >> 4) & 0x0F;

    // Set lower column address
    TFT_WriteCommand(0x00 | lower_column);
    // Set higher column address
    TFT_WriteCommand(0x10 | higher_column);
    // Set page start address
    TFT_WriteCommand(0xB0 | page);
  }
}


/**
  @brief Clean all display
*/
static void TFT_Clear(void)
{
  u8 x, y;
  for(y = 0; y < 8; y++)
  {
    TFT_WriteCommand(0xB0 + y);            
    TFT_WriteCommand(0x00);              
    TFT_WriteCommand(0x10);             
    for(x = 0; x < 0x80; x++)
    {
      TFT_WriteData(0x00);
    }
  }
}

/**
  @brief Init TFT
*/
static int TFT_DisplayInit(void)
{
	TFT_SetValueGpioRES(0x00);                         
	TFT_SetValueGpioRES(0x01);
	msleep(1);                          
	/*
	** Commands to initialize the SSD_1306 OLED Display
	*/
	TFT_WriteCommand(0xAE); // Entire Display OFF
	TFT_WriteCommand(0xD5); // Set Display Clock Divide Ratio and Oscillator Frequency
	TFT_WriteCommand(0x80); // Default Setting for Display Clock Divide Ratio and Oscillator Frequency that is recommended
	TFT_WriteCommand(0xA8); // Set Multiplex Ratio
	TFT_WriteCommand(0x3F); // 64 COM lines
	TFT_WriteCommand(0xD3); // Set display offset
	TFT_WriteCommand(0x00); // 0 offset
	TFT_WriteCommand(0x40); // Set first line as the start line of the display
	TFT_WriteCommand(0x8D); // Charge pump
	TFT_WriteCommand(0x14); // Enable charge dump during display on
	TFT_WriteCommand(0x20); // Set memory addressing mode
	TFT_WriteCommand(0x00); // Horizontal addressing mode
	TFT_WriteCommand(0xA1); // Set segment remap with column address 127 mapped to segment 0
	TFT_WriteCommand(0xC8); // Set com output scan direction, scan from com63 to com 0
	TFT_WriteCommand(0xDA); // Set com pins hardware configuration
	TFT_WriteCommand(0x12); // Alternative com pin configuration, disable com left/right remap
	TFT_WriteCommand(0x81); // Set contrast control
	TFT_WriteCommand(0x80); // Set Contrast to 128
	TFT_WriteCommand(0xD9); // Set pre-charge period
	TFT_WriteCommand(0xF1); // Phase 1 period of 15 DCLK, Phase 2 period of 1 DCLK
	TFT_WriteCommand(0xDB); // Set Vcomh deselect level
	TFT_WriteCommand(0x20); // Vcomh deselect level ~ 0.77 Vcc
	TFT_WriteCommand(0xA4); // Entire display ON, resume to RAM content display
	TFT_WriteCommand(0xA6); // Set Display in Normal Mode, 1 = ON, 0 = OFF
	TFT_WriteCommand(0x2E); // Deactivate scroll
	TFT_WriteCommand(0xAF); // Display ON in normal mode

	//Clear the display
	TFT_Clear();
    TFT_SetCursor(0, 0);
	return 0;
}

/**
 * @brief Update timing between to ADC reads
 */
static ssize_t my_write(struct file *File, const char *user_buffer, size_t count, loff_t *offs) 
{
	char lcd_buffer[128];
	int to_copy, not_copy, delta;

	/* Get amount of data to copy */
	to_copy = min(count, sizeof(lcd_buffer));

	/* Copy data to user */
	not_copy = copy_from_user(lcd_buffer, user_buffer, to_copy);
		
	/* Calculate data */
	delta = to_copy - not_copy;

	/* Display the new data */
	TFT_Clear();
    TFT_SetCursor(0, 0);
	for(int i = 0; i < to_copy - 1; i++)
	{
		TFT_PrintChar(lcd_buffer[i]);
	}

	return delta;
}

static struct proc_ops fops = {
	.proc_write = my_write,
};

/**
 * @brief This function is called on loading the driver 
 */
static int my_spi_probe(struct spi_device *spi)
{
	printk("Hello, Kernel!\n");
	printk("dt_spi - Now I am in the Probe function!\n");

    spi_oled = spi;
		
	/* Creating procfs file */
	proc_file = proc_create("myspi", 0666, NULL, &fops);
	if(proc_file == NULL) 
	{
		printk("dt_spi - Error creating /proc/myspi\n");
		return -ENOMEM;
	}

	/* Register GPIO Res and Dc */
	TFT_GPIO_Init_ResDc();
    /* TFT Init */
	TFT_DisplayInit();
	msleep(1);
    TFT_SetCursor(1,40);  
	TFT_String("Loi Tran");
	TFT_SetCursor(4,15);  
	TFT_String("Test Display TFT");
    msleep(2000);
    TFT_SetCursor(0, 0);
	
	return 0;
}

/**
 * @brief This function is called on unloading the driver 
 */
static void my_spi_remove(struct spi_device *spi)
{
	/* DeRegister GPIO Res and Dc*/
	TFT_GPIO_DeInit_ResDc();
	/* Clear display */
	TFT_SetCursor(0, 0);
	TFT_Clear();
	printk("dt_spi - Now I am in the Remove function!\n");
	proc_remove(proc_file);
	printk("Bye, Kernel!\n");
}

/* This will create the init and exit function automatically */
module_spi_driver(my_driver);