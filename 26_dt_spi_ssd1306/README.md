SCK - D0  -> GPIO11  
MOSI - D1 -> GPIO10  
MISO      -> Not Connect  
CS        -> CE0 GPIO8  
RES       -> GPIO24  
DC        -> GPIO23  
---------------------------------  
  
sudo raspi-config  
-> 3 Interface Options  
--> I3 SPI  
---> Yes -> Ok  
-> Finish  
---------------------------------  
  
sudo nano /boot/firmware/config.txt   
add: dtoverlay=devicetree  