#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/string.h>

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("Demonstartion for dynamic memory management in a LKM");

struct driver_data {
	u8 version;
	char data[64];
};

u32 *ptr1;
struct driver_data *ptr2;

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init my_init(void)
{
	printk("Hello, Kernel!\n");

	/* kmalloc: Allocates memory without initializing it */
	ptr1 = kmalloc(sizeof(u32), GFP_KERNEL);
	if(ptr1 == NULL)
	{
		printk("alloc_test - Out of memory!\n");
		return -1;
	}
	printk("alloc_test - *ptr1: 0x%x\n", *ptr1);
	*ptr1 = 0x0000abcd;
	printk("alloc_test - *ptr1: 0x%x\n", *ptr1);
	kfree(ptr1);

	/* kzalloc: Allocates memory and automatically initializes it to ZERO */
	ptr1 = kzalloc(sizeof(u32), GFP_KERNEL);
	if(ptr1 == NULL)
	{
		printk("alloc_test - Out of memory!\n");
		return -1;
	}
	printk("alloc_test - *ptr1: 0x%x\n", *ptr1);
	*ptr1 = 0x0000abcd;
	printk("alloc_test - *ptr1: 0x%x\n", *ptr1);
	kfree(ptr1);

	ptr2 = kzalloc(sizeof(struct driver_data), GFP_KERNEL);
	if(ptr2 == NULL) 
	{
		printk("alloc_test - Out of memory!\n");
		return -1;
	}
	printk("alloc_test - ptr2->version: %d\n", ptr2->version);
	printk("alloc_test - ptr2->data: %s\n", ptr2->data);
	ptr2->version = 123;
	strcpy(ptr2->data, "This is a test string for my LKM");
	printk("alloc_test - ptr2->version: %d\n", ptr2->version);
	printk("alloc_test - ptr2->data: %s\n", ptr2->data);
	kfree(ptr2);

	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit my_exit(void)
{
	printk("Bye, Kernel!\n");
}

module_init(my_init);	/* Register the constructor function */
module_exit(my_exit);	/* Register the exit function */
