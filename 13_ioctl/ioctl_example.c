#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/ioctl.h>

#include "ioctl_test.h"

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("A simple example for ioctl in a LKM");

#define MYMAJOR 90 // Device number

/**
	@brief This is function called, When the device file is opened
*/
static int driver_open(struct inode *device_file, struct file *instance)
{
	printk("dev_nr - open was called!\n");
	return 0;
}

/**
	@brief This is function called, When the device file is closed
*/
static int driver_close(struct inode *device_file, struct file *instance)
{
	printk("dev_nr - close was called!\n");
	return 0;
}

/* Global variable for reading and writing */
int32_t answer = 42;

static long int my_ioctl(struct file *file, unsigned cmd, unsigned long arg)
{
	struct mystruct test;

	switch (cmd)
	{
		case WR_VALUE:
			if(copy_from_user(&answer, (int32_t *) arg, sizeof(answer)))
			{
				printk("ioctl_example - Error copying data from user!\n");
			}
			else
			{
				printk("ioctl_example - Update the answer to %d\n", answer);
			}
		break;

		case RD_VALUE:
			if(copy_to_user((int32_t *) arg, &answer, sizeof(answer)))
			{
				printk("ioctl_example - Error copying data from user!\n");
			}
			else
			{
				printk("ioctl_example - The answer was copied!\n");
			}
		break;

		case GREETER:
			if(copy_from_user(&test, (struct mystruct *) arg, sizeof(answer)))
			{
				printk("ioctl_example - Error copying data from user!\n");
			}
			else
			{
				printk("ioctl_example - %d greets to %s\n", test.repeat, test.name);
			}
		break;
	}

	return 0;
}

static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = driver_open,
	.release = driver_close,
	.unlocked_ioctl = my_ioctl
};

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init Module_Init(void)
{
	int retval;
	printk("Hello, Kernel!\n");

	/* register device nr. */
	/* register_chrdev: used to register some character devices */
	retval = register_chrdev(MYMAJOR, "my_dev_nr", &fops);
	if(retval == 0)
	{
		printk("dev_nr - register Device number Major: %d, Minor: %d\n", MYMAJOR, 0);
	}
	else if(retval > 0)
	{
		printk("dev_nr - register Device number Major: %d, Minor: %d\n", retval>>20, retval&0xfffff);
	}
	else
	{
		printk("Could not register device number!\n");
		return -1;
	}

	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit Module_Exit(void)
{
	/* unregister_chrdev: used to deregister some character devices */
	unregister_chrdev(MYMAJOR, "my_dev_nr");
	printk("Bye, Kernel!\n");
}

module_init(Module_Init);	/* Register the constructor function */
module_exit(Module_Exit);	/* Register the exit function */
