#include <linux/module.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("A simple LKM for a gpio interrupt");

/* variable contains pin number o interrupt controller to which GPIO 17 is mapped to*/
unsigned int irq_number;

/**
	@brief Interrupt service routine is called, when interrupt is triggered
*/
static irq_handler_t gpio_irq_handle(unsigned int irq, void *dev_id, struct pt_regs *regs)
{
	printk("gpio_irq: Interrupt was triggered and ISR was called!\n");
	return (irq_handler_t) IRQ_HANDLED;
}

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init my_init(void)
{
	printk("Hello, Kernel!\n");

	/* Setup the gpio */
	if(gpio_request(17, "irq-gpio-17"))
	{
		printk("Error!\nCan not allocate GPIO 17\n");
		return -1;
	}

	/* Set GPIO 17 direction */
	if(gpio_direction_input(17))
	{
		printk("Error!\nCan not set GPIO 17 to input!\n");
		gpio_free(17);
		return -1;
	}

	/* Setup the interrupt */
	irq_number = gpio_to_irq(17);

	if(request_irq(irq_number, (irq_handler_t) gpio_irq_handle, IRQF_TRIGGER_RISING, "my_gpio_irq", NULL) != 0)
	{
		printk("Error!\nCan not request interrupt nr. : %d\n", irq_number);
		gpio_free(17);
		return -1;
	}

	printk("Done!\n");
	printk("GPIO 17 is mapped to IRQ Nr. : %d\n", irq_number);
	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit my_exit(void)
{
	printk("gpio_irq: Unloading module ...\n");
	free_irq(irq_number, NULL);
	gpio_free(17);
}

module_init(my_init);	/* Register the constructor function */
module_exit(my_exit);	/* Register the exit function */
