#include <linux/module.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/slab.h>

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("Demonstration for lists in the kernel");

struct my_data {
	struct list_head list;
	char text[64];
};
LIST_HEAD(my_list);

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init my_init(void)
{
	struct my_data *tmp;
	struct my_data *next;
	struct list_head *ptr;

	printk("Hello, Kernel!\n");

	/* Let's create a list with three elements */
	tmp = kmalloc(sizeof(struct my_data), GFP_KERNEL);
	strcpy(tmp->text, "Once element");
	list_add_tail(&tmp->list, &my_list);

	tmp = kmalloc(sizeof(struct my_data), GFP_KERNEL);
	strcpy(tmp->text, "Second element");
	list_add_tail(&tmp->list, &my_list);

	tmp = kmalloc(sizeof(struct my_data), GFP_KERNEL);
	strcpy(tmp->text, "Three element");
	list_add_tail(&tmp->list, &my_list);

	list_for_each_prev(ptr, &my_list) {
		tmp = list_entry(ptr, struct my_data, list);
		printk("mylist - Element text: %s\n", tmp->text);
	}

	/* Free memory */
	list_for_each_entry_safe(tmp, next, &my_list, list) {
		list_del(&tmp->list);
		kfree(tmp);
	}
	printk("mylist - Freeing memory done!\n");

	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit my_exit(void)
{
	printk("Bye, Kernel!\n");
}

module_init(my_init);	/* Register the constructor function */
module_exit(my_exit);	/* Register the exit function */
