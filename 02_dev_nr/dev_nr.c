#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("Registers a device nr.  and implement some callback functions");

#define MYMAJOR 90 // Device number

/**
	@brief This is function called, When the device file is opened
*/
static int driver_open(struct inode *device_file, struct file *instance)
{
	printk("dev_nr - open was called!\n");
	return 0;
}

/**
	@brief This is function called, When the device file is closed
*/
static int driver_close(struct inode *device_file, struct file *instance)
{
	printk("dev_nr - close was called!\n");
	return 0;
}

static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = driver_open,
	.release = driver_close
};

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init Module_Init(void)
{
	int retval;
	printk("Hello, Kernel!\n");

	/* register device nr. */
	/* register_chrdev: used to register some character devices */
	retval = register_chrdev(MYMAJOR, "my_dev_nr", &fops);
	if(retval == 0)
	{
		printk("dev_nr - register Device number Major: %d, Minor: %d\n", MYMAJOR, 0);
	}
	else if(retval > 0)
	{
		printk("dev_nr - register Device number Major: %d, Minor: %d\n", retval>>20, retval&0xfffff);
	}
	else
	{
		printk("Could not register device number!\n");
		return -1;
	}

	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit Module_Exit(void)
{
	/* unregister_chrdev: used to deregister some character devices */
	unregister_chrdev(MYMAJOR, "my_dev_nr");
	printk("Bye, Kernel!\n");
}

module_init(Module_Init);	/* Register the constructor function */
module_exit(Module_Exit);	/* Register the exit function */
