#include <linux/module.h>
#include <linux/init.h>


/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("A simple LKM to demonstrate the use of parameter");

/* Kernel Module's parameters */
static unsigned int gpio_nr = 12;
static char *device_name = "testdevice";

module_param(gpio_nr, uint, S_IRUGO);
module_param(device_name, charp, S_IRUGO);

MODULE_PARM_DESC(gpio_nr, "Nr. of GPIO to use in this LKM");
MODULE_PARM_DESC(device_name, "Device name to use in this LKM");

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init my_init(void)
{
	printk("Hello, Kernel!\n");
	printk("gpio_nr = %u\n", gpio_nr);
	printk("device_name = %s\n", device_name);
	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit my_exit(void)
{
	printk("Bye, Kernel!\n");
}

module_init(my_init);	/* Register the constructor function */
module_exit(my_exit);	/* Register the exit function */
