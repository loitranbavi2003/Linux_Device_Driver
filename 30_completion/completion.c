#include <linux/module.h>
#include <linux/init.h>
#include <linux/jiffies.h>
#include <linux/timer.h>
#include <linux/completion.h>

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("A simple test for completions");

/* variable for timer */
static struct timer_list my_timer;
static struct completion comp;

void timer_callback(struct timer_list *data)
{
	printk("mycompletion - timer expired\n");
}

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init my_init(void)
{
	int status;
	printk("Hello, Kernel!\n");

	/* Initialize timer */
	timer_setup(&my_timer, timer_callback, 0);

	/* Initialize completion */
	init_completion(&comp);

	printk("mycompletion - Start ther timer the first timer. Timer: 40ms\n");
	mod_timer(&my_timer, jiffies + msecs_to_jiffies(40));
	status = wait_for_completion_timeout(&comp, msecs_to_jiffies(100));
	if(!status)
	{
		printk("mycompletion - Completion timed out!\n");
	}

	reinit_completion(&comp);
	printk("mycompletion - Start ther timer the first timer. Timer: 400ms\n");
	mod_timer(&my_timer, jiffies + msecs_to_jiffies(400));
	status = wait_for_completion_timeout(&comp, msecs_to_jiffies(100));
	if(!status)
	{
		printk("mycompletion - Completion timed out!\n");
	}

	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit my_exit(void)
{
	del_timer(&my_timer);
	printk("Bye, Kernel!\n");
}

module_init(my_init);	/* Register the constructor function */
module_exit(my_exit);	/* Register the exit function */
