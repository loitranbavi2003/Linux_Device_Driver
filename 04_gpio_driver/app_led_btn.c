#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define DEVICE_PATH "/dev/my_gpio_driver"

int main() 
{
    int fd;
    char value_led;
    char buffer[3]; // Buffer để lưu giá trị đọc được
    ssize_t bytes_read;

    fd = open(DEVICE_PATH, O_RDWR);
    if (fd < 0) 
    {
        perror("Failed to open the device.");
        return 1;
    }

    while(1)
    {
        bytes_read = read(fd, buffer, sizeof(buffer)-1);
        if (bytes_read < 0) 
        {
            perror("Failed to read from the device.");
            close(fd);
            return 1;
        }

        value_led = buffer[0];
        if (write(fd, &value_led, sizeof(value_led)) < 0) 
        {
            perror("Failed to write to the device.");
            close(fd);
            return 1;
        }
    }

    close(fd);
    return 0;
}
