#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define DEVICE_PATH "/dev/my_gpio_driver"

int main() 
{
    int fd;
    char value;

    fd = open(DEVICE_PATH, O_RDWR);
    if (fd < 0) 
    {
        perror("Failed to open the device.");
        return 1;
    }

    while(1)
    {
        value = '0';
        if (write(fd, &value, sizeof(value)) < 0) 
        {
            perror("Failed to write to the device.");
            close(fd);
            return 1;
        }
        printf("Set value Led: 0\n");
        usleep(1000000);

        value = '1';
        if (write(fd, &value, sizeof(value)) < 0) 
        {
            perror("Failed to write to the device.");
            close(fd);
            return 1;
        }
        printf("Set value Led: 1\n");
        usleep(1000000);
    }

    close(fd);
    return 0;
}
