#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define DEVICE_PATH "/dev/my_gpio_driver"

int main() 
{
    int fd;
    char buffer[3]; // Buffer để lưu giá trị đọc được
    ssize_t bytes_read;

    fd = open(DEVICE_PATH, O_RDWR);
    if (fd < 0) 
    {
        perror("Failed to open the device.");
        return 1;
    }

    while(1)
    {
        // Đọc giá trị từ thiết bị
        bytes_read = read(fd, buffer, sizeof(buffer)-1);
        if (bytes_read < 0) 
        {
            perror("Failed to read from the device.");
            close(fd);
            return 1;
        }

        buffer[bytes_read] = '\0';
        printf("Value read from device: %s\n", buffer); 

        usleep(1000000);
    }

    close(fd);
    return 0;
}
