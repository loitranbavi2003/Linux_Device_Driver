#include <stdio.h>
#include <fcntl.h>

int main(int argc,char **argv)
{
    int fd; 
    unsigned char buf[168] = {0};
    int i;
    fd = open("/dev/oled",O_RDWR);   /* 打开字符设备/dev/oled */
    if(fd < 0){
        printf(" can't open /dev/oled \n\r");
        return -1;
    }
 
    write(fd,argv[1],168);        /* 将接收的第二个字符串传入内核的字符设备 */
 
    return 0;
 
}
