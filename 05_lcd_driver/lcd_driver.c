#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/gpio.h>
#include <linux/delay.h>

/* Meta Information */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Loi Tran 4 GNU/Linux");
MODULE_DESCRIPTION("A driver to write to a LCD text display");

/* Variables for device and device class */
static dev_t my_device_nr;
static struct class *my_class;
static struct cdev my_device;

#define DRIVER_NAME "lcd"
#define DRIVER_CLASS "MyModuleClass"

/* LCD char buffer */
static char lcd_buffer[17];

/* Pinout for LCD Display */
unsigned int gpios[] = {
	17, /* Register Select Pin */
		/* Read/Write - GND*/
	27, /* Enable Pin */
	22, /* Data Pin 0*/
	10, /* Data Pin 1*/
	9, 	/* Data Pin 2*/
	11, /* Data Pin 3*/
	5, 	/* Data Pin 4*/
	6, 	/* Data Pin 5*/
	13, /* Data Pin 6*/
	19, /* Data Pin 7*/
};

#define REGISTER_SELECT gpios[0]

/**
 * @brief generates a pulse on the enable signal
 */
void LCD_Enable(void) 
{
	gpio_set_value(gpios[1], 1);
	msleep(5);
	gpio_set_value(gpios[1], 0);
}

/**
 * @brief set the 8 bit data bus
 * @param data: Data to set
 */
void LCD_Send8Bit(char data) 
{
	for(int i=0; i<8; i++)
	{
		gpio_set_value(gpios[i+2], ((data) & (1<<i)) >> i);
	}
	LCD_Enable();
	msleep(5);
}

/**
 * @brief send a command to the LCD
 *
 * @param data: command to send
 */
void LCD_SendCommand(uint8_t data) 
{
 	gpio_set_value(REGISTER_SELECT, 0);	/* RS to Instruction */
	LCD_Send8Bit(data);
}

/**
	@brief Init LCD
*/
void LCD_Init(void)
{
	LCD_SendCommand(0x38);	/* Set the display for 8 bit data interface */
	LCD_SendCommand(0xf);	/* Turn display on, turn cursor on, set cursor blinking */
	// LCD_SendCommand (0x0C);	/* Display ON Cursor OFF */
	// LCD_SendCommand (0x06);	/* Auto Increment cursor */
	// LCD_SendCommand (0x80);	/* Cursor at home position */
	LCD_SendCommand(0x1);	/* Clear display */
}

/**
 * @brief send a data to the LCD
 *
 * @param data: command to send
 */
void LCD_Data(uint8_t data) 
{
 	gpio_set_value(REGISTER_SELECT, 1);	/* RS to data */
	LCD_Send8Bit(data);
}

/**
	@brief Send string data
*/
void LCD_SendString(char *c)
{
	while(*c)
	{
		LCD_Data(*c);
		c++;
	}	
}

/**
	@brief Goto xy
*/
void LCD_Gotoxy (char row, char pos)  
{
	if (row == 0)
	{
		LCD_SendCommand((pos & 0x0F)|0x80);
	}
	else if (row == 1)
	{
		LCD_SendCommand((pos & 0x0F)|0xC0);
	}
}

/**
 * @brief Write data to buffer
 */
static ssize_t driver_write(struct file *File, const char *user_buffer, size_t count, loff_t *offs) 
{
	int to_copy, not_copied, delta;

	/* Get amount of data to copy */
	to_copy = min(count, sizeof(lcd_buffer));

	/* Copy data to user */
	not_copied = copy_from_user(lcd_buffer, user_buffer, to_copy);

	/* Calculate data */
	delta = to_copy - not_copied;

	/* Set the new data to the display */
	LCD_SendCommand(0x1);
	LCD_SendString(lcd_buffer);

	// for(int i = 0; i < to_copy; i++)
	// {
	// 	LCD_Data(lcd_buffer[i]);
	// }
		
	return delta;
}

/**
 * @brief This function is called, when the device file is opened
 */
static int driver_open(struct inode *device_file, struct file *instance) {
	printk("dev_nr - open was called!\n");
	return 0;
}

/**
 * @brief This function is called, when the device file is opened
 */
static int driver_close(struct inode *device_file, struct file *instance) {
	printk("dev_nr - close was called!\n");
	return 0;
}

static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = driver_open,
	.release = driver_close,
	.write = driver_write
};

/**
 * @brief This function is called, when the module is loaded into the kernel
 */
static int __init ModuleInit(void) 
{
	int i;
	char *names[] = {"REGISTER_SELECT", "ENABLE_PIN", "DATA_PIN0", "DATA_PIN1", "DATA_PIN2", "DATA_PIN3", "DATA_PIN4", "DATA_PIN5", "DATA_PIN6", "DATA_PIN7"};
	printk("Hello, Kernel!\n");

	/* Allocate a device nr */
	if( alloc_chrdev_region(&my_device_nr, 0, 1, DRIVER_NAME) < 0) {
		printk("Device Nr. could not be allocated!\n");
		return -1;
	}
	printk("read_write - Device Nr. Major: %d, Minor: %d was registered!\n", my_device_nr >> 20, my_device_nr && 0xfffff);

	/* Create device class */
	if((my_class = class_create(THIS_MODULE, DRIVER_CLASS)) == NULL) {
		printk("Device class can not be created!\n");
		goto ClassError;
	}

	/* create device file */
	if(device_create(my_class, NULL, my_device_nr, NULL, DRIVER_NAME) == NULL) {
		printk("Can not create device file!\n");
		goto FileError;
	}

	/* Initialize device file */
	cdev_init(&my_device, &fops);

	/* Regisering device to kernel */
	if(cdev_add(&my_device, my_device_nr, 1) == -1) {
		printk("lcd-driver - Registering of device to kernel failed!\n");
		goto AddError;
	}

	/* Initialize GPIOs */
	printk("lcd-driver - GPIO Init\n");
	for(i = 0; i < 10; i++) {
		if(gpio_request(gpios[i], names[i])) {
			printk("lcd-driver - Error Init GPIO %d\n", gpios[i]);
			goto GpioInitError;
		}
	}

	printk("lcd-driver - Set GPIOs to output\n");
	for(i = 0; i < 10; i++) {
		if(gpio_direction_output(gpios[i], 0)) {
			printk("lcd-driver - Error setting GPIO %d to output\n", i);
			goto GpioDirectionError;
		}
	}

	/* Init the display */
	LCD_Init();
	LCD_Gotoxy(0, 2);
	LCD_SendString("Hello World!");
	LCD_Gotoxy(1,3);
	LCD_SendString("AIoT Lab!");

	return 0;
GpioDirectionError:
	i = 9;
GpioInitError:
	for(; i>=0; i--)
		gpio_free(gpios[i]);
AddError:
	device_destroy(my_class, my_device_nr);
FileError:
	class_destroy(my_class);
ClassError:
	unregister_chrdev_region(my_device_nr, 1);
	return -1;
}

/**
 * @brief This function is called, when the module is removed from the kernel
 */
static void __exit ModuleExit(void) 
{
	LCD_SendCommand(0x1);	/* Clear the display */
	for(int i = 0; i < 10; i++)
	{
		gpio_set_value(gpios[i], 0);
		gpio_free(gpios[i]);
	}
	cdev_del(&my_device);
	device_destroy(my_class, my_device_nr);
	class_destroy(my_class);
	unregister_chrdev_region(my_device_nr, 1);
	printk("Goodbye, Kernel\n");
}

module_init(ModuleInit);
module_exit(ModuleExit);


