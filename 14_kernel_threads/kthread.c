#include <linux/module.h>
#include <linux/init.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include <linux/delay.h>

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("A simple example for threads in a LKM");

/* Global variable for the threads */
static struct task_struct *kthread_1;
static struct task_struct *kthread_2;
static int t1 = 1, t2 = 2;

/**
	@brief Function which will be executed by the threads
	@param thread_nr Pointer to number of the thread
*/
int thread_function(void *thread_nr)
{
	unsigned int i = 0;
	int t_nr = *(int *) thread_nr;

	/* 
		working loop 
		kthread_should_stop() returns true when the thread is requested to stop
	*/
	while(!kthread_should_stop())
	{
		printk("kthread - Thread %d is execution! Counter val: %d\n", t_nr, i++);
		msleep(t_nr * 1000);
	}

	printk("kthread - Thread %d finished execution!\n", t_nr);
	return 0;
}

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init my_init(void)
{
	printk("Hello, Kernel!\n");
	printk("kthread - Init threads!\n");

	/* 
		kthread_create(): is a function that creates a kernel thread 
							but does not start it immediately.
		wake_up_process(): wakes up the newly created thread
	*/

	kthread_1 = kthread_create(thread_function, &t1, "kthread_1");
	if(kthread_1 != NULL)
	{
		/* Let's start the thread */
		wake_up_process(kthread_1);
		printk("kthread - Thread 1 was create and is runing now!\n");
	}
	else
	{
		printk("kthread - Thread 1 could not be created\n");
		return -1;
	}

	/* 
		kthread_create() + wake_up_process() = kthread_run() 
	*/

	kthread_2 = kthread_run(thread_function, &t2, "kthread_2");
	if(kthread_2 != NULL)
	{
		printk("kthread - Thread 2 was create and is runing now!\n");
	}
	else
	{
		printk("kthread - Thread 2 could not be created\n");
		kthread_stop(kthread_1);
		return -1;
	}

	printk("kthread - Both threads are running now!\n");

	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit my_exit(void)
{
	printk("kthread - Stop both threads\n");
	kthread_stop(kthread_1);
	kthread_stop(kthread_2);
	printk("Bye, Kernel!\n");
}

module_init(my_init);	/* Register the constructor function */
module_exit(my_exit);	/* Register the exit function */
