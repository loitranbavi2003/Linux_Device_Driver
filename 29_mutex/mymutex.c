#include <linux/module.h>
#include <linux/init.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/mutex.h>

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("A simple example for threads in a LKM");

/* Global variable for the threads */
static struct task_struct *kthread_1;
static struct task_struct *kthread_2;
static struct mutex lock;
static int t1 = 1, t2 = 2;

/**
	@brief Function which will be executed by the threads
	@param thread_nr Pointer to number of the thread
*/
int thread_function(void *thread_nr)
{
	int delay[] = {0, 1000, 500};
	int t_nr = *(int *) thread_nr;
	
	/*
		This function acquires ownership of the mutex, which means it locks the mutex. 
		If the mutex is already locked by another thread, 
			the calling thread will be put to sleep until it successfully acquires the lock.
	*/
	printk("mymutex - Thread %d is execution!\n", t_nr);
	
	mutex_lock(&lock);
	printk("mymutex - Thread %d is in critical section!\n", t_nr);
	msleep(delay[t_nr]);
	printk("mymutex - Thread %d is leaving the critical section!\n", t_nr);
	mutex_unlock(&lock);

	printk("mymutex - Thread %d finished execution!\n", t_nr);
	return 0;
}

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init my_init(void)
{
	printk("Hello, Kernel!\n");
	printk("mymutex - Init threads!\n");

	mutex_init(&lock);

	/* 
		kthread_create(): is a function that creates a kernel thread 
							but does not start it immediately.
		wake_up_process(): wakes up the newly created thread
	*/

	kthread_1 = kthread_create(thread_function, &t1, "kthread_1");
	if(kthread_1 != NULL)
	{
		/* Let's start the thread */
		wake_up_process(kthread_1);
		printk("mymutex - Thread 1 was create and is runing now!\n");
	}
	else
	{
		printk("mymutex - Thread 1 could not be created\n");
		return -1;
	}

	/* 
		kthread_create() + wake_up_process() = kthread_run() 
	*/

	kthread_2 = kthread_run(thread_function, &t2, "kthread_2");
	if(kthread_2 != NULL)
	{
		printk("mymutex - Thread 2 was create and is runing now!\n");
	}
	else
	{
		printk("mymutex - Thread 2 could not be created\n");
		kthread_stop(kthread_1);
		return -1;
	}

	printk("mymutex - Both threads are running now!\n");

	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit my_exit(void)
{
	printk("mymutex - Stop both threads\n");
	printk("Bye, Kernel!\n");
}

module_init(my_init);	/* Register the constructor function */
module_exit(my_exit);	/* Register the exit function */
