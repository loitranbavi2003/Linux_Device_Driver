#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("An example for reading and writing to a file in your filesystem from a driver");

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init my_init(void)
{
	struct file *file;
	char data[128] = "This is test file!\n";
	ssize_t len;

	printk("Hello, Kernel!\n");

	/* Open the file */
	file = filp_open("/tmp/loi", O_RDWR | O_CREAT, 0644);
	if(!file)
	{
		printk("file_access - Error opening file\n");
		return -1;
	}

	/* Write to the file */
	len = kernel_write(file, data, sizeof(data), &file->f_pos);
	if(len < 0)
	{
		printk("file_access - Error writing to file: %ld\n", len);
		filp_close(file, NULL);
	}
	printk("file_access - Wrote %ld bytes to file\n", len);	

	/* Read it back */
	memset(data, 0, sizeof(data));
	file->f_pos = 0;

	len = kernel_read(file, data, sizeof(data), &file->f_pos);
	if(len < 0)
	{
		printk("file_access - Error reading the file: %ld\n", len);
		filp_close(file, NULL);
	}

	printk("file_access - Read %ld bytes: %s\n", len , data);
	filp_close(file, NULL);

	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit my_exit(void)
{
	printk("file_access - Unloading driver\n");
	printk("Bye, Kernel!\n");
}

module_init(my_init);	/* Register the constructor function */
module_exit(my_exit);	/* Register the exit function */
