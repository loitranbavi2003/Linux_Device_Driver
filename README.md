## Getting started  
sudo apt install raspberrypi-kernel-headers  
  
sudo insmod mymodule.ko  
lsmod | grep mymodule  
dmesg | tail  
modinfo mymodule.ko  
sudo rmmod mymodule  
  
## Pi 4 Pinout  
![](raspberry-pi-3-pinout.jpg)
