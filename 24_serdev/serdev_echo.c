#include <linux/module.h>
#include <linux/init.h>
#include <linux/mod_devicetable.h>
#include <linux/property.h>
#include <linux/platform_device.h>
#include <linux/of_device.h>
#include <linux/serdev.h>

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("A simple loopback driver for an UART port");

/* Declate the probe and remove functions */
static int serdev_echo_prode(struct serdev_device *serdev);
static void serdev_echo_remove(struct serdev_device *serdev);

static struct of_device_id serdev_echo_ids[] = {
	{
		.compatible = "brightlight,echodev",
	}, { /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, serdev_echo_ids);

static struct serdev_device_driver serdev_echo_driver = {
	.probe = serdev_echo_prode,
	.remove = serdev_echo_remove,
	.driver = {
		.name = "serdev_echo",
		.of_match_table = serdev_echo_ids,
	},
};

/**
	@brief Callback is called whenever a character is received
*/
static int serdev_echo_recv(struct serdev_device *serdev, const unsigned char *buffer, size_t size)
{
	printk("serdev_echo - Received %ld bytes with \"%s\"\n", size, buffer);
	/* Resend the message received via the serial port. */
	return serdev_device_write_buf(serdev, buffer, size);
}

static const struct serdev_device_ops serdev_echo_ops = {
	.receive_buf = serdev_echo_recv,
};

/**
	@brief This function is called on loading the driver
*/
static int serdev_echo_prode(struct serdev_device *serdev)
{
	int status;
	printk("serdev_echo - Now I am in the probe function!\n");

	/*
		I'm informing the 'serdev' framework that whenever data is received on this particular 'serdev' device, 
		it should invoke the 'serdev_echo_recv' function to handle the received data
	*/
	serdev_device_set_client_ops(serdev, &serdev_echo_ops);
	status = serdev_device_open(serdev);
	if(status)
	{
		printk("serdev_echo - Error opening serial port!\n");
		return -1;
	}

	serdev_device_set_baudrate(serdev, 9600);
	/* Set up flow control for the device's serial port. */
	serdev_device_set_flow_control(serdev, false);
	/* 	
		Set parity for the device's serial port. 
		Parity is a mechanism to check for errors in data communication.
	*/
	serdev_device_set_parity(serdev, SERDEV_PARITY_NONE);

	status = serdev_device_write_buf(serdev, "Type something: ", sizeof("Type something: "));
	printk("serdev_echo - Wrote %d bytes.\n", status);

	return 0;
}	

/**
	@brief This function is called on unloading the driver
*/
static void serdev_echo_remove(struct serdev_device *serdev)
{
	printk("serdev_echo - Now I am in the remove function\n");
	serdev_device_close(serdev);
}

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init my_init(void)
{
	printk("Hello, Kernel!\n");

	printk("serdev_echo - Loading the driver...\n");
	if(serdev_device_driver_register(&serdev_echo_driver))
	{
		printk("serdev_echo - Error Could not load driver!\n");
		return -1;
	}

	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit my_exit(void)
{
	printk("serdev_echo - Unload driver\n");
	serdev_device_driver_unregister(&serdev_echo_driver);
	printk("Bye, Kernel!\n");
}

module_init(my_init);	/* Register the constructor function */
module_exit(my_exit);	/* Register the exit function */
