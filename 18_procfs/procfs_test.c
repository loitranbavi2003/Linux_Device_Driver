#include <linux/module.h>
#include <linux/init.h>
#include <linux/proc_fs.h>

/* Description Information */
MODULE_LICENSE("GPL");						/*General Public License*/
MODULE_AUTHOR("Loi Tran GNU/Linux");
MODULE_DESCRIPTION("Module creates a folder and file in procfs and implements read and write callbacks");

/* global variables for procfs folder and file */
static struct proc_dir_entry *proc_folder;
static struct proc_dir_entry *proc_file;

/**
	@brief Read data out of the buffer
*/
static ssize_t driver_read(struct file *File, char *user_buffer, size_t count, loff_t *offs)
{
	char text[] = "Hello from a procfs file!\n";
	int to_copy, not_copied, delta;

	/* Get amount of data to copy */
	to_copy = min(count, sizeof(text));

	/* Copy data to user */
	not_copied = copy_to_user(user_buffer, text, to_copy);

	/* Calculate data */
	delta = to_copy - not_copied;

	return delta;
}

/**
	@brief Write data to buffer
*/
static ssize_t driver_write(struct file *File, const char *user_buffer, size_t count, loff_t *offs)
{
	char text[255];
	int to_copy, not_copied, delta;

	/* clear text*/
	memset(text, 0, sizeof(text));

	/* Get amount of data to copy */
	to_copy = min(count, sizeof(text));

	/* Copy data to user */
	not_copied = copy_from_user(text, user_buffer, to_copy);
	printk("procfs_test - You have writen %s to me\n", text);
	
	/* Calculate data */
	delta = to_copy - not_copied;

	return delta;
}

static struct proc_ops fops = {
	.proc_read = driver_read,
	.proc_write = driver_write
};

/**
	@brief This is function called, When the module is loaded into the kernel
*/
static int __init Module_Init(void)
{
	printk("Hello, Kernel!\n");

	/* /proc/hello/dummy */

	proc_folder = proc_mkdir("hello", NULL);
	if(proc_folder == NULL)
	{
		printk("procfs_test - Error creating /proc/hello\n");
		return -ENOMEM;
	}

	proc_file = proc_create("dummy", 0666, proc_folder, &fops);
	if(proc_file == NULL)
	{
		printk("procfs_test - Error creating /proc/hello/dummy\n");
		proc_remove(proc_folder);
		return -ENOMEM;
	}

	printk("procfs_test - Created /proc/hello/dummy\n");

	return 0;
}

/**
	@brief This is function called, When the module is removed form the kernel
*/
static void __exit Module_Exit(void)
{
	printk("procfs_test - Removing /proc/hello/dummy\n");
	proc_remove(proc_file);
	proc_remove(proc_folder);
	printk("Bye, Kernel!\n");
}

module_init(Module_Init);	/* Register the constructor function */
module_exit(Module_Exit);	/* Register the exit function */
